package com.erick.service;

import java.util.*;


public class OlahNilai implements ProsesOlahNilai{
    @Override
    public double mean(List<Integer> list)throws IndexOutOfBoundsException, ArithmeticException{
        return list.stream().mapToDouble(d -> d).average().orElse(0.0);
    }

    @Override
    public double median(List<Integer> list)  {

            Arrays.sort(new List[]{list});
            double median;
            if (list.size() % 2 == 0)
                median = ((double) list.get(list.size() / 2) + (double) list.get(list.size() / 2 - 1)) / 2 ;
            else median = (double) list.get(list.size() / 2);
            return median;


    }
    @Override
    public double modus(List<Integer> list) throws IndexOutOfBoundsException, ArithmeticException {
        HashMap<Integer, Integer> hm = new HashMap<>();
        int max = 1;
        int temp = 0;

        for (Integer integer : list) {

            if (hm.get(integer) != null) {

                int count = hm.get(integer);
                count++;
                hm.put(integer, count);

                if (count > max) {
                    max = count;
                    temp = integer;
                }
            } else hm.put(integer, 1);
        }
        return temp;
    }


    @Override
    public Map<Integer, Integer> freq(List<Integer> array)  throws IndexOutOfBoundsException, ArithmeticException{
        Set<Integer> distinct = new HashSet<>(array);
        Map<Integer, Integer> mMap = new HashMap<>();

        for (Integer s : distinct) {
            mMap.put(s, Collections.frequency(array, s));
        }
        return mMap;
    }
}
