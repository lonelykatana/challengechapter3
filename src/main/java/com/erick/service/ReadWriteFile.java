package com.erick.service;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface ReadWriteFile {
    void olahNilai(String filePath, String filePathOlahNilai, String delimiter) throws IOException;

    void writeFile(String filePathWrite, String filePath, String delimiter) throws IOException;
}
