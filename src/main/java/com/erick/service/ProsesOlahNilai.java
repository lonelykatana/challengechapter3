package com.erick.service;

import java.util.List;
import java.util.Map;

public interface ProsesOlahNilai {
    double mean(List<Integer> list);

    double median(List<Integer> list);

    double modus(List<Integer> list);

    Map<Integer, Integer> freq(List<Integer> array);
}
