package com.erick.service;

import java.util.InputMismatchException;

public class CloseMenu extends Menu {
    @Override
    public void showMenu() {
        MainMenu main = new MainMenu();
        System.out.println("File telah diproses! Tentukan pilihan anda: \n1. Kembali ke menu utama \n0.Exit");
        try {
            switch (input()) {
                case 0:
                    System.out.println("Program sedang ditutup");
                    System.exit(0);
                    break;
                case 1:
                    main.showMenu();
                    break;
                default:
                    System.out.println("Pilihan tidak dikenali! silahkan pilih lagi");
                    this.showMenu();
                    break;
            }
        } catch (InputMismatchException|NullPointerException e){
            System.out.println(e);
        }

    }
}
