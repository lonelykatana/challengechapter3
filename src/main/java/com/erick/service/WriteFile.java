package com.erick.service;


import java.io.*;
import java.util.*;
import java.util.logging.Logger;

public class WriteFile implements ReadWriteFile {


    //write frekuensi
    @Override
    public void olahNilai(String filePath, String filePatOlahNilai, String delimiter) throws IOException {
        try {
            File file2 = new File(filePath);
            FileReader fr = new FileReader(file2);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] tempArr;
            List<Integer> listInt = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                tempArr = line.split(delimiter);

                for (int i = 0; i < tempArr.length; i++) {
                    if (i == 0) {

                    } else {
                        String temp = tempArr[i];
                        int intTemp = Integer.parseInt(temp);
                        listInt.add(intTemp);
                    }
                }
            }
            File file3 = new File(filePatOlahNilai);
            if (file3.createNewFile()) {
                System.out.println("FIle sudah dibuat");
            }
            FileWriter writer = new FileWriter(file3);
            BufferedWriter bwr = new BufferedWriter(writer);
            OlahNilai olahNilai = new OlahNilai();
            Map<Integer, Integer> hMap = olahNilai.freq(listInt);
            Set<Integer> key = hMap.keySet();

            bwr.write("Berikut Hasil Pengolahan Nilai:\n");
            bwr.write(" \n");
            bwr.write("Nilai\t\t\t\t" + "|\t\t" + "Frekuensi" + "\n");

            for (Integer nilai : key) {
                bwr.write(nilai + "\t\t\t\t\t" + "|\t\t" + hMap.get(nilai) + "\n");
            }
            bwr.flush();
            br.close();
            bwr.close();


        } catch (FileNotFoundException e) {
            throw e;
        }
    }


    //write mean, median , modus
    @Override
    public void writeFile(String filePathWrite, String filePath, String delimiter) throws IOException, IndexOutOfBoundsException {
        try {
            //READ FILE
            File file = new File(filePath);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] tempArr;

            List<Integer> listInt = new ArrayList<>();

            while ((line = br.readLine()) != null) {
                tempArr = line.split(delimiter);

                for (int i = 0; i < tempArr.length; i++) {
                    if (i == 0) {

                    } else {
                        String temp = tempArr[i];
                        int intTemp = Integer.parseInt(temp);
                        listInt.add(intTemp);
                    }
                }
            }


            //WRITE FILE
            File file1 = new File(filePathWrite);
            if (file1.createNewFile()) {
                System.out.println("File sudah dibuat");
            }
            FileWriter writer = new FileWriter(file1);
            BufferedWriter bwr = new BufferedWriter(writer);

            OlahNilai olahNilai = new OlahNilai();
            bwr.write("Pengolahan Nilai");
            bwr.newLine();
            bwr.write("Mean : " + String.format("%.2f", olahNilai.mean(listInt)));
            bwr.newLine();
            bwr.write("Mean : " + olahNilai.median(listInt));
            bwr.newLine();
            bwr.write("Modus : " + olahNilai.modus(listInt));
            bwr.flush();
            br.close();
            bwr.close();
        } catch (FileNotFoundException e) {
            throw e;

        } catch (IndexOutOfBoundsException e){
            throw e;
        }


    }


}

