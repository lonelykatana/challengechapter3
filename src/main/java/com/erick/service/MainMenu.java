package com.erick.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InputMismatchException;

public class MainMenu extends Menu {
    private static final String FILE_PATH_READ = "src/main/resources/data_sekolah.txt";
    private static final String FILE_PATH_WRITE = "src/main/resources/PengolahanNilai";
    private static final String FILE_PATH_WRITE_OLAH_NILAI = "src/main/resources/OlahNilai.txt";

    @Override
    public void showMenu() {
        ReadWriteFile file = new WriteFile();
        CloseMenu close = new CloseMenu();
        System.out.println("Pilih menu :\n1.Frekuensi Nilai \n2.Mean,Modus,dan Median \n0.exit ");
        try {
            switch (input()) {
                case 0:
                    System.out.println("Program sedang ditutup");
                    System.exit(0);
                    break;
                case 1:
                    file.olahNilai(FILE_PATH_READ, FILE_PATH_WRITE_OLAH_NILAI, ";");
                    close.showMenu();
                    break;
                case 2:
                    file.writeFile(FILE_PATH_WRITE, FILE_PATH_READ, ";");
                    close.showMenu();
                    break;
                default:
                    System.out.println("Pilihan tidak dikenali! silahkan pilih lagi");
                    this.showMenu();
                    break;
            }
        } catch (InputMismatchException | NullPointerException  | IndexOutOfBoundsException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
