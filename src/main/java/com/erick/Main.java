package com.erick;

import com.erick.controller.ReadWriteFileController;
import com.erick.model.KelasNilai;
import com.erick.service.ReadWriteFile;

public class Main {
    public static void main(String[] args) {
        ReadWriteFileController rwfc = new ReadWriteFileController();
        rwfc.startReadWriteProcess();
    }
}
