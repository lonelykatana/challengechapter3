import com.erick.service.MainMenu;
import com.erick.service.WriteFile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InputMismatchException;

public class WriteFileTest {
    WriteFile wf = new WriteFile();

    @Test
    @DisplayName("Negative Test - WriteFile.olahNilai()")
    void olahNilaiTest() {
        String test_file_path = "src/main/resources/data_sekola.txt"; //path salah
        String test_FILE_PATH_WRITE_OLAH_NILAI = "src/main/resources/OlahNilai.txt";
        String test_delimiter = ";";
        Assertions.assertThrows(FileNotFoundException.class, () -> wf.olahNilai(test_file_path, test_FILE_PATH_WRITE_OLAH_NILAI, test_delimiter));
    }

    @Test
    @DisplayName("Negative Test - WriteFile.writeFile()")
    void writeFileTest() {
        String test_FILE_PATH_WRITE = "src/main/resources/PengolahanNila"; //path salah
        String test_file_path = "src/main/resources/data_sekola.txt";
        String test_delimiter = ";";
        Assertions.assertThrows(FileNotFoundException.class, () -> wf.writeFile(test_FILE_PATH_WRITE, test_file_path, test_delimiter));

    }


}
