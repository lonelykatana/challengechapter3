import com.erick.service.OlahNilai;
import com.erick.service.WriteFile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OlahNilaiTest {

    OlahNilai olahNilai = new OlahNilai();

    @Test
    @DisplayName("Positive Test - Mean")
    void mean() {
        double test = 8d;
        List<Integer> angka = Arrays.asList(8, 8, 8, 8, 8);
        Assertions.assertEquals(test, olahNilai.mean(angka));
    }

    @Test
    @DisplayName("Positive Test - Median")
    void median() {
        double test = 5d;
        List<Integer> angka = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        Assertions.assertEquals(test, olahNilai.median(angka));
    }

    @Test
    @DisplayName("Positive Test - Modus")
    void modus() {
        double test = 3d;
        List<Integer> angka = Arrays.asList(2, 3, 1, 3, 3, 5, 6, 4, 3, 3);
        Assertions.assertEquals(test, olahNilai.modus(angka));
    }
}
